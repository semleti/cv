import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { EditRoutingModule } from './edit-routing.module';
import { UserComponent } from './components/user/user.component';
import { CvComponent } from './components/cv/cv.component';
import { SharedModule } from '@app/shared/shared.module';
import { SectionComponent } from './components/section/section.component';
import { FieldsComponent } from './components/fields/fields.component';

@NgModule({
  declarations: [UserComponent, CvComponent, SectionComponent, FieldsComponent],
  imports: [
    CommonModule,
    EditRoutingModule,
    FormsModule,
    SharedModule,
    ReactiveFormsModule,
    MatIconModule
  ]
})
export class EditModule { }
