import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './components/user/user.component';
import { CvComponent } from './components/cv/cv.component';

const routes: Routes = [
  { path: '', component: UserComponent},
  { path: 'profile/:uid', component: UserComponent},
  { path: 'cv/:cv', component: CvComponent},
  { path: 'cv', component: CvComponent},
  { path: ':uid', component: UserComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditRoutingModule { }
