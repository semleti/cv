import { Component, OnInit, Input } from '@angular/core';
import { ControlContainer, NgForm } from '@angular/forms';

@Component({
  selector: 'cv-edit-fields',
  templateUrl: './fields.component.html',
  styleUrls: ['./fields.component.css'],
  viewProviders: [ { provide: ControlContainer, useExisting: NgForm } ]
})
export class FieldsComponent implements OnInit {

  @Input() fields;
  @Input() name;
  @Input() modelGroup;

  constructor() { }

  ngOnInit() {
  }

}
