import { Component, OnInit, Input } from '@angular/core';
import { ControlContainer, NgForm } from '@angular/forms';

@Component({
  selector: 'cv-edit-section',
  templateUrl: './section.component.html',
  styleUrls: ['./section.component.css'],
  viewProviders: [ { provide: ControlContainer, useExisting: NgForm } ]
})
export class SectionComponent implements OnInit {

  @Input() section;
  @Input() name;
  @Input() modelGroup;

  constructor() { }

  ngOnInit() {
  }

  addField() {
    this.section.template.fields["temp"]={};
  }

}
