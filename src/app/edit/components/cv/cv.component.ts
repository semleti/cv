import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ErrorLogService } from '@app/services/error-log.service';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Title } from '@angular/platform-browser';
import { AuthService } from '@app/services/auth.service';

import { take } from 'rxjs/operators'

@Component({
  selector: 'cv-edit-cv',
  templateUrl: './cv.component.html',
  styleUrls: ['./cv.component.css']
})
export class CvComponent implements OnInit {

  originalName: string;
  data: any = {letters:[],sections:[]};
  uid: string;
  docRef: AngularFirestoreDocument;
  letters: any[] = [{title:"1",img:"a"}];

  constructor(private _route: ActivatedRoute, private auth: AuthService, private titleService: Title, private afs: AngularFirestore, private errorLog: ErrorLogService) {
    this._route.params
      .subscribe(params => {this.auth.user.subscribe(u => {
        if(u) {
          this.uid = u.uid;
            if(params.cv) {
              this.data.name = params.cv;
              this.originalName = params.cv;
          
              this.docRef = this.afs.doc(`users/${this.uid}/cvs/${params.cv}`);
              this.docRef.valueChanges().subscribe(d => {
                this.data = d;
              })
            }
          }
        })
      });
  }

  ngOnInit() {
  }

  addLetter() {
    this.data.letters.push({});
  }

  submit(v) {
    //TODO: add CV to users/${userId}.cvs
    this.docRef = this.afs.doc(`users/${this.uid}/cvs/${this.data.name}`);
    this.afs.doc(`users/${this.uid}/cvs/${this.data.name}`).set(this.data).then(() => {
      this.afs.doc(`users/${this.uid}`).get().subscribe(d => {
        let dat = d.data();
        let present = false;
        for(let i = 0; i < dat.cvs.length; i++) {
          if(dat.cvs[i].name === this.data.name) {
            present = true;
            break;
          }
        }
        if(!present) {
          dat.cvs.push({name:this.data.name,title:this.data.title});
          this.afs.doc(`users/${this.uid}`).update(dat);
        }
      });
      console.log('Le CV a bien été enregistré');
    }).catch(error => this.errorLog.report(error));
  }

  deleteCV() {
    //TODO: remove CV from users/${userId}.cvs
    if(confirm("Supprimer votre CV? Cette action est irrévocable!")) {
      this.docRef.delete().then(() => {
        this.afs.doc(`users/${this.uid}`).get().subscribe(d => {
          let dat = d.data();
          for(let index = 0; index < dat.cvs.length; index++) {
            if(dat.cvs[index].name === this.originalName) {
              dat.cvs.splice(dat.cvs.indexOf(index),1);
              break;
            }
          }
          this.afs.doc(`users/${this.uid}`).update(dat);
        });
        console.log("CV supprimé");
      }).catch(error => this.errorLog.report(error));
    }
  }

  addSection() {
    this.data.sections.push({template:{fields:[]}});
  }

  upgrade() {
    for (let section of this.data.sections) {
      console.log({section});
      let fields = [];
      for(let f of Object.keys(section.template.fields)) {
        section.template.fields[f].name = f;
        fields.push(section.template.fields[f]);
      }
      section.template.fields = fields;
    }
    this.submit(null);
  }

}
