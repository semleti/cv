import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { AngularFirestore, AngularFirestoreDocument, DocumentData } from '@angular/fire/firestore';
import { AuthService } from '@app/services/auth.service';
import { Observable } from 'rxjs';
// import { FormGroup, FormControl, FormsModule } from '@angular/forms';

import { take } from 'rxjs/operators'
import { ErrorLogService } from '@app/services/error-log.service';

@Component({
  selector: 'cv-edit-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  user: Observable<DocumentData>;
  userData: any = {address:{}};
  userRef: AngularFirestoreDocument;

  constructor(private _route: ActivatedRoute, public auth: AuthService, private titleService: Title, private afs: AngularFirestore, private errorLog: ErrorLogService) {
    this._route.params
      .subscribe(params => {
        if(params.uid) {
          this.getUser(params.uid);
        }
        else {
          this.auth.user.subscribe(user => {
            if(user) {
              this.getUser(user.uid);
            }
          })
        }
      }
    );
  }

  getUser(uid) {
    this.userRef = this.afs.doc(`users/${uid}`);
    this.user = this.userRef.valueChanges();
    this.user.pipe(take(1)).subscribe(d => {
      this.userData = d;
      if(!this.userData.address)
        this.userData.address = {};
    });
  }

  deleteUser() {
    if(confirm("Supprimer votre compte? Cette action est irrévocable!")) {
      this.userRef.delete().then(() => {
        console.log("Utilisateur supprimé");
        this.auth.logout([""]);
      })
      .catch(error => {
        this.errorLog.report(error);
      });
    }
  }

  ngOnInit() {
  }

  submit(value) {
    this.userRef.update(this.cleanUp(value)).then(() => {
      console.log("done"); 
    }).catch(e => {this.errorLog.report(e)});
  }

  cleanUp(src) {
    if(typeof(src) !== "object")
      return src;
    let dst = {};
    Object.keys(src).forEach(key => {
        if(src[key] !== undefined) {
          dst[key] = this.cleanUp(src[key]);
        }
    });
    return dst;
  }

}
