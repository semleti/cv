import { Component, OnInit, Input } from '@angular/core';
import { Template } from '../goku/goku.component';

@Component({
  selector: 'cv-section',
  templateUrl: './section.component.html',
  styleUrls: ['./section.component.css']
})
export class SectionComponent implements OnInit {

  @Input() maxNumber: number;
  @Input() elements: any[];
  @Input() template: Template;
  @Input('print-columns') print_columns;

  constructor() { }

  ngOnInit() {
  }

}
