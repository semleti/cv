import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'cv-contact-info',
  templateUrl: './contact-info.component.html',
  styleUrls: ['./contact-info.component.css']
})
export class ContactInfoComponent implements OnInit {

  @Input() person;

  constructor() { }

  ngOnInit() {
  }

}
