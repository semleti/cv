import { Component, OnInit, Input } from '@angular/core';

export interface FieldDescriptor {
  area: string,
  type: string,
  name: string
}

export interface Template {
  areas: string,
  columns: string,
  rows: string,
  columnGap: string,
  fields: FieldDescriptor[]
}

export interface Fields {
    [key: string]: any | {url: string, value: any};
}

@Component({
  selector: 'cv-goku',
  templateUrl: './goku.component.html',
  styleUrls: ['./goku.component.css']
})
export class GokuComponent implements OnInit {

  @Input() fields: Fields;
  @Input() template: Template;

  constructor() { }

  ngOnInit() {
  }

  getTemplateField(field) {
    return this.template.fields.find(f => f.name === field.key);
  }

}
