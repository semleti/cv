import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import { AuthService } from '@app/services/auth.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';

import { take } from 'rxjs/operators'

@Component({
  selector: 'cv-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(public auth: AuthService, private _location: Location, private afs: AngularFirestore, private router: Router) { }

  ngOnInit() {
  }

  googleLogin() {
    this.auth.googleLogin().then(() =>
      this.auth.user.pipe(take(1)).subscribe(user => {
          this.afs.doc(`users/${user.uid}`).get().subscribe(f => {
          if(f.exists)
            this.back();
          else {
            this.afs.doc(`users/${user.uid}`).set({cvs:[]});
            this.router.navigate([`edit/${user.uid}`]);
          }
          });
      })
    )
  }

  back() {
    this._location.back();
  }

}
