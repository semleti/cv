import { Component, OnInit, Input } from '@angular/core';

export interface Letter {
  img: string,
  name: string,
  title: string
}

@Component({
  selector: 'cv-recommandation-letter',
  templateUrl: './recommandation-letter.component.html',
  styleUrls: ['./recommandation-letter.component.css']
})
export class RecommandationLetterComponent implements OnInit {

  @Input() letter: Letter;

  constructor() { }

  ngOnInit() {
  }

}
