import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecommandationLetterComponent } from './recommandation-letter.component';

describe('RecommandationLetterComponent', () => {
  let component: RecommandationLetterComponent;
  let fixture: ComponentFixture<RecommandationLetterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecommandationLetterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecommandationLetterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
