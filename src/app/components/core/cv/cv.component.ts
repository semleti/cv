import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

import { map, take } from 'rxjs/operators'

@Component({
  selector: 'cv-cv',
  templateUrl: './cv.component.html',
  styleUrls: ['./cv.component.css']
})
export class CvComponent implements OnInit {

  person;
  personId;
  cv;
  
  constructor(private _route: ActivatedRoute, private titleService: Title, private afs: AngularFirestore) {
    this._route.params
      .subscribe(params => {
        this.personId = this.afs.collection('users', ref => ref.limit(1).where('slug','==',params.user))
          .snapshotChanges().pipe(map(actions => {
          let id = actions[0].payload.doc.id;
          let data = actions[0].payload.doc.data();
          return {id, data};
        } ));
        this.person = this.personId.pipe(map(pId => {
          return (pId as any).data
        }));
        this.personId.subscribe( ({id,data}) => {
          this.titleService.setTitle( `CV: ${data.firstname} ${data.lastname}`);
          this.cv = this.afs.doc(`users/${id}/cvs/${(params.cv || data.defaultCV)}`).valueChanges();
        })
      })
   }

  ngOnInit() {
  }


}
