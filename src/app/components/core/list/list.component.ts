import { Component, OnInit, ViewChild, Input, ElementRef, Renderer2, AfterViewInit, HostBinding } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'cv-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit, AfterViewInit {

  @ViewChild('ngContent') ngContent : ElementRef;
  @Input() maxNumber = -1;
  @Input('print-columns') print_columns = 4;

  toggled: boolean = false;

  constructor(private renderer : Renderer2, private sanitizer: DomSanitizer) { }

  ngOnInit() {
  }
  
  @HostBinding("attr.style")
  public get valueAsStyle(): any {
    return this.sanitizer.bypassSecurityTrustStyle(`--print-columns: ${this.print_columns}`);
  }

  ngAfterViewInit() {
    this.updateList();
    
  }

  toggle() {
    this.toggled = !this.toggled;
    this.updateList();
  }

  //TODO: improve
  updateList() {
    if(this.maxNumber > 0) {
      for(let i = 1; i < this.ngContent.nativeElement.childNodes.length; i++) {
        this.renderer.setStyle(
          this.ngContent.nativeElement.childNodes[i],
          'display',
          i <= this.maxNumber || this.toggled ? null : 'none'
        );
      }
    }
  }

}
