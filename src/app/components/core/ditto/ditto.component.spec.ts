import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DittoComponent } from './ditto.component';

describe('DittoComponent', () => {
  let component: DittoComponent;
  let fixture: ComponentFixture<DittoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DittoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DittoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
