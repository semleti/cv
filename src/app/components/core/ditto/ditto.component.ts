import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'cv-ditto',
  templateUrl: './ditto.component.html',
  styleUrls: ['./ditto.component.css']
})
export class DittoComponent implements OnInit {

  @Input() value;
  @Input() type: string;

  constructor() { }

  ngOnInit() {
  }

}
