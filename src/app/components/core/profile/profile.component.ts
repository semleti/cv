import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from '@app/services/auth.service';

import { take, map } from 'rxjs/operators'
import { Observable } from 'rxjs';

@Component({
  selector: 'cv-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  user: Observable<any>;

  constructor(private _route: ActivatedRoute, private titleService: Title, public auth: AuthService, private afs: AngularFirestore) {
    this._route.params
      .subscribe(params => {
        if(!params.user) {
          this.auth.user.pipe().subscribe(u => {
            this.user = this.afs.doc(`users/${u.uid}`).valueChanges();
          });
        }
        else {
          this.user = this.afs.collection('users', ref => ref.limit(1).where('slug','==',params.user)).valueChanges().pipe(map(us => us[0]));
        }
      });
   }

  ngOnInit() {
  }

}
