import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'cv-array',
  templateUrl: './array.component.html',
  styleUrls: ['./array.component.css']
})
export class ArrayComponent implements OnInit {

  @Input() array: any; //TODO: string[] | {separator: string, values: string[]};

  constructor() { }

  ngOnInit() {
  }

}
