import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'cv-mastery',
  templateUrl: './mastery.component.html',
  styleUrls: ['./mastery.component.css']
})
export class MasteryComponent implements OnInit {

  @Input() mastery: number;

  constructor() { }

  ngOnInit() {
  }

}
