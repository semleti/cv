import { Component, OnInit, ElementRef, Renderer2, Input } from '@angular/core';
import { Fields } from '@app/components/core/goku/goku.component';

@Component({
  selector: 'cv-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  
  visible: boolean = true;
  @Input() element: Fields;
  @Input() template: string;

  constructor(private elt: ElementRef, private renderer: Renderer2) { }

  ngOnInit() {
  }

  

  toggle(v) {
    this.visible = v;
    if(this.visible)
      this.renderer.removeClass(this.elt.nativeElement, "hide-at-print");
    else
      this.renderer.addClass(this.elt.nativeElement, "hide-at-print");
  }

}
