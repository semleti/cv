import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'cv-visibility-toggle',
  templateUrl: './visibility-toggle.component.html',
  styleUrls: ['./visibility-toggle.component.css']
})
export class VisibilityToggleComponent implements OnInit {

  visible: boolean = true;
  @Output() visibilityChange = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit() {
  }

  toggle() {
    this.visible = !this.visible;
    this.visibilityChange.emit(this.visible);
  }

}