import { Component, OnInit, Input } from '@angular/core';

export interface Dates {
  type: string,
  start: string,
  end?: string
}

@Component({
  selector: 'cv-dates',
  templateUrl: './dates.component.html',
  styleUrls: ['./dates.component.css']
})
export class DatesComponent implements OnInit {

  @Input() value: Dates;

  constructor() { }

  ngOnInit() {
  }

  diff(start: string, end: string) {
    return this.toYears(new Date(end).getTime() - new Date(start).getTime());
  }

  diffToday(birthdate: string) {
    return this.toYears(Date.now() - new Date(birthdate).getTime());
  }

  toYears(time: number) {
    return Math.abs(Math.floor((time) / 1000 / (3600 * 24) / 365.25));
  }

}
