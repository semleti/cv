import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CvMiniatureComponent } from './components/cv-miniature/cv-miniature.component'

@NgModule({
  declarations: [CvMiniatureComponent],
  exports: [
    CvMiniatureComponent
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
