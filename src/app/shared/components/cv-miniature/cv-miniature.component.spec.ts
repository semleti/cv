import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CvMiniatureComponent } from './cv-miniature.component';

describe('CvMiniatureComponent', () => {
  let component: CvMiniatureComponent;
  let fixture: ComponentFixture<CvMiniatureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CvMiniatureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CvMiniatureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
