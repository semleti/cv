import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'cv-cv-miniature',
  templateUrl: './cv-miniature.component.html',
  styleUrls: ['./cv-miniature.component.css']
})
export class CvMiniatureComponent implements OnInit {

  @Input() cv;

  constructor() { }

  ngOnInit() {
  }

}
