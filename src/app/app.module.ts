import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatToolbarModule, MatDividerModule,MatListModule,MatCardModule,MatIconModule,MatButtonModule,MatButtonToggleModule} from '@angular/material';
import { HomeComponent } from '@core/home/home.component';
import { NavComponent } from '@core/nav/nav.component';
import { AboutComponent } from '@core/about/about.component';
import { TimesDirective } from '@directives/times.directive';
import { MasteryComponent } from '@shared/mastery/mastery.component';
import { ContactInfoComponent } from '@core/contact-info/contact-info.component';
import { RecommandationLetterComponent } from '@core/recommandation-letter/recommandation-letter.component';
import { ListComponent } from '@core/list/list.component';

import { FirstletterUppercasePipe } from './pipes/firstletter-uppercase.pipe';
import { ReplacePipe } from './pipes/replace.pipe';
import { VisibilityToggleComponent } from './components/shared/visibility-toggle/visibility-toggle.component';
import { CardComponent } from './components/shared/card/card.component';
import { SectionComponent } from './components/core/section/section.component';
import { CvComponent } from './components/core/cv/cv.component';
import { GokuComponent } from './components/core/goku/goku.component';
import { DittoComponent } from './components/core/ditto/ditto.component';
import { DatesComponent } from './components/shared/dates/dates.component';
import { ArrayComponent } from './components/shared/array/array.component';
import { LoginComponent } from './components/core/login/login.component'
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { ProfileComponent } from './components/core/profile/profile.component';
import { SharedModule } from './shared/shared.module';
import { FindWithPropertyPipe } from './pipes/findWithProperty.pipe';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavComponent,
    AboutComponent,
    TimesDirective,
    MasteryComponent,
    ContactInfoComponent,
    FirstletterUppercasePipe,
    RecommandationLetterComponent,
    ListComponent,
    ReplacePipe,
    VisibilityToggleComponent,
    CardComponent,
    SectionComponent,
    CvComponent,
    GokuComponent,
    DittoComponent,
    DatesComponent,
    ArrayComponent,
    LoginComponent,
    ProfileComponent,
    FindWithPropertyPipe
  ],
  exports: [
    
  ],
  imports: [
    SharedModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatDividerModule,
    MatListModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule,
    MatButtonToggleModule,
    AngularFireModule.initializeApp({
      apiKey: "AIzaSyBEcAZtQbpqSB8rcD8mZLj7SwEpfyvo2Z8",
      authDomain: "mon-cv-1.firebaseapp.com",
      databaseURL: "https://mon-cv-1.firebaseio.com",
      projectId: "mon-cv-1",
      storageBucket: "mon-cv-1.appspot.com",
      messagingSenderId: "1096202660220"
    }),
    AngularFireAuthModule,
    AngularFirestoreModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
