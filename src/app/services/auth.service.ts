import { Injectable } from '@angular/core';
import { Router } from '@angular/router'

import * as fb from 'firebase/app'
import { AngularFireAuth } from '@angular/fire/auth'
import { AngularFirestore, AngularFirestoreDocument} from '@angular/fire/firestore'

import { Observable } from 'rxjs';

interface User {
  uid: string,
  email: string,
  photoURL?: string,
  displayName?: string
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<User>;
  uid: string;

  constructor(private afAuth: AngularFireAuth, private afs: AngularFirestore, private router: Router) {
    this.user = this.afAuth.authState;
    this.user.subscribe(user => {
      if(user) {
        this.uid = user.uid;
      }
      else
        this.uid = '';
    })
  }

  googleLogin() {
    const provider = new fb.auth.GoogleAuthProvider();
    return this.oAuthLogin(provider);
  }

  oAuthLogin(provider) {
    return this.afAuth.auth.signInWithPopup(provider)
      .then((credential) => {
        
      });
  }

  logout(redirect: any[]) {
    this.afAuth.auth.signOut();
    if(redirect)
      this.router.navigate(redirect);
  }

}
