import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { AngularFirestore } from '@angular/fire/firestore';

import { take } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class ErrorLogService {

  constructor(private auth: AuthService, private afs: AngularFirestore) { }

  report(error) {
    let msg = {};
    let now = Date.now();
    let data: any = {error: error.toString()};
    if(this.auth.uid)
      data.user = this.auth.uid;
    msg[now] = data;
    this.afs.doc("logs/erros").update(msg).then(() => {
      console.log("following error has been reported:")
      console.log(error);
    }).catch(e => {
      console.log(e);
      console.log("previous error prevented the following to be reported:")
      console.log(error);
    })
  }
}
