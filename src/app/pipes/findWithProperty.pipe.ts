import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'findWithProperty'
})
export class FindWithPropertyPipe implements PipeTransform {

  transform(value: any, propertyName: string, propertyValue: any): any {
    return value.find(e => {e[propertyName] === propertyValue});
  }

}