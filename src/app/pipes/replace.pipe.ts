import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'replace'
})
export class ReplacePipe implements PipeTransform {

  transform(value: string, substr: string, newSubstr: string): any {
    return value.replace(new RegExp(substr, 'g'),newSubstr);
  }

}
