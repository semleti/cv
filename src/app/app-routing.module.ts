import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '@core/home/home.component';
import { AboutComponent } from './components/core/about/about.component';
import { CvComponent } from './components/core/cv/cv.component';
import { LoginComponent } from './components/core/login/login.component';
import { ProfileComponent } from './components/core/profile/profile.component';
const routes: Routes = [
  { path: 'about', component: AboutComponent},
  { path: 'login', component: LoginComponent},
  { path: 'profile', component: ProfileComponent},
  { path: 'edit', loadChildren: './edit/edit.module#EditModule'},
  { path: 'profile/:user', component: ProfileComponent},
  { path: ':user', component: CvComponent},
  { path: ':user/:cv', component: CvComponent},
  { path: '', redirectTo: '/roman.seiler',pathMatch: 'full'},
  { path: '', component: HomeComponent},
  { path: '**', redirectTo: '/'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
 