import { Component } from '@angular/core';

// These imports load individual services into the firebase namespace.
import 'firebase/auth';
import 'firebase/firestore';

@Component({
  selector: 'cv-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor() {
  }

}
